//
//  iOS_Challenge_IoasysTests.swift
//  iOS Challenge IoasysTests
//
//  Created by Kalil Holanda on 23/11/20.
//

import XCTest
@testable import iOS_Challenge_Ioasys

final private class LoginPresenterTests: XCTestCase {
    
    var sut: LoginPresenter!
    var loginViewSpy: LoginViewSpy!
    var loginServiceStub: LoginServiceStub!
    
    override func setUp() {
        loginServiceStub = LoginServiceStub()
        loginViewSpy = LoginViewSpy()
        sut = LoginPresenter(loginView: loginViewSpy, webservice: loginServiceStub)
    }

    override func tearDown() {
        sut = nil
        loginServiceStub = nil
        loginViewSpy = nil
    }

    func testInvalidEmail_shouldReturnFalse() {
        XCTAssertFalse(sut.isValidEmail(email: "email"))
    }
    
    func testValidEmail_shouldReturnTrue() {
        XCTAssertTrue(sut.isValidEmail(email: "email@teste.com"))
    }
    
    func testInvalidPassword_shouldReturnFalse() {
        XCTAssertFalse(sut.isValidPassword(password: "1"))
    }
    
    func testValidPassword_shouldReturnTrue() {
        XCTAssertTrue(sut.isValidPassword(password: "123456"))
    }
    
    func testInvalidEmail_shouldCallOnlyInvalidEmail() {
        sut.login(email: "123456", password: "")
        
        XCTAssertTrue(loginViewSpy.hasCalledInvalidEmail)
        XCTAssertFalse(loginViewSpy.hasCalledErrorLogin)
        XCTAssertFalse(loginViewSpy.hasCalledSuccessfulLogin)
    }
    
    func testValidEmailAndInvalidPassword_shouldCallOnlyErrorLogin() {
        sut.login(email: "email@email.com", password: "123")
        
        XCTAssertTrue(loginViewSpy.hasCalledErrorLogin)
        XCTAssertFalse(loginViewSpy.hasCalledInvalidEmail)
        XCTAssertFalse(loginViewSpy.hasCalledSuccessfulLogin)
    }
    
    func testValidEmailAndValidPassword_customErrorReturn() {
        loginServiceStub.customError = .failedRequest
        sut = LoginPresenter(loginView: loginViewSpy, webservice: loginServiceStub)
        
        sut.login(email: "email@email.com", password: "1234567")
        
        XCTAssertTrue(loginViewSpy.hasCalledErrorLogin)
        XCTAssertFalse(loginViewSpy.hasCalledInvalidEmail)
        XCTAssertFalse(loginViewSpy.hasCalledSuccessfulLogin)
    }
    
    func testValidEmailAndValidPassword_shouldCallOnlySuccessfulLogin() {
        sut.login(email: "email@email.com", password: "1234567")
        
        XCTAssertTrue(loginViewSpy.hasCalledSuccessfulLogin)
        XCTAssertFalse(loginViewSpy.hasCalledInvalidEmail)
        XCTAssertFalse(loginViewSpy.hasCalledErrorLogin)
    }
}
