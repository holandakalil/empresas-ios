//
//  EnterprisePresenterTests.swift
//  iOS Challenge IoasysTests
//
//  Created by Kalil Holanda on 26/11/20.
//

import XCTest
@testable import iOS_Challenge_Ioasys

final private class EnterprisePresenterTests: XCTestCase {
    
    var sut: EnterprisePresenter!
    var enterpriseViewSpy: EnterpriseViewSpy!
    var enterpriseServiceStub: EnterpriseServiceStub!
    
    override func setUp() {
        enterpriseViewSpy = EnterpriseViewSpy()
        enterpriseServiceStub = EnterpriseServiceStub()
        sut = EnterprisePresenter(enterpriseView: enterpriseViewSpy, webservice: enterpriseServiceStub)
    }

    override func tearDown() {
        sut = nil
        enterpriseViewSpy = nil
        enterpriseServiceStub = nil
    }
    
    func testGetEnterprisesCountReturn() {
        XCTAssertEqual(sut.getEnterprisesCount(), 0, "Expected to return zero")
    }
    
    func testGetEnterprises() {
        XCTAssertNil(sut.getEnterprise(for: 0), "Expected to return nil")
    }
    
    func testFetchEnterprises_customErrorReturn() {
        enterpriseServiceStub.customError = .failedRequest
        sut = EnterprisePresenter(enterpriseView: enterpriseViewSpy, webservice: enterpriseServiceStub)
        
        sut.fetchEnterprises()
        
        XCTAssertTrue(enterpriseViewSpy.hasCalledErrorRequest, "expected to call errorResquest")
        XCTAssertFalse(enterpriseViewSpy.hasCalledSuccessfulRequest, "called successful when not expected")
    }
    
    func testFetchEnterprises_luccessfulReturn() {
        sut.fetchEnterprises()
        
        XCTAssertTrue(enterpriseViewSpy.hasCalledSuccessfulRequest, "expected to call successful")
        XCTAssertFalse(enterpriseViewSpy.hasCalledErrorRequest, "called errorResquest when not expected")
    }
}
