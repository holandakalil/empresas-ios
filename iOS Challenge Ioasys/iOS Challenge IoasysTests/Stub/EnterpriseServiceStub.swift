//
//  EnterpriseServiceStub.swift
//  iOS Challenge IoasysTests
//
//  Created by Kalil Holanda on 26/11/20.
//

import XCTest
@testable import iOS_Challenge_Ioasys

class EnterpriseServiceStub: EnterpriseServiceProtocol {
    
    private var urlSession: URLSession
    private var urlString: String
    required init(urlString: String = "", urlSession: URLSession = .shared) {
        self.urlString = urlString
        self.urlSession = urlSession
    }
    
    var customError: CustomError?
    
    func getEnterprises(completion: @escaping (Result<Enterprises, CustomError>) -> ()) {
        if let error = customError {
            completion(.failure(error))
        } else {
            completion(.success(Enterprises(enterprises: [])))
        }
    }
}
