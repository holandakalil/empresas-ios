//
//  LoginServiceStub.swift
//  iOS Challenge IoasysTests
//
//  Created by Kalil Holanda on 24/11/20.
//

import XCTest
@testable import iOS_Challenge_Ioasys

class LoginServiceStub: LoginServiceProtocol {
    
    private var urlSession: URLSession
    private var urlString: String
    required init(urlString: String = "", urlSession: URLSession = .shared) {
        self.urlString = urlString
        self.urlSession = urlSession
    }
    
    var customError: CustomError?
    
    func login(email: String, password: String, completion: @escaping (Result<UserModel, CustomError>) -> ()) {
        if let error = customError {
            completion(.failure(error))
        } else {
            completion(.success(UserModel(email: email, password: password)))
        }
    }
}
