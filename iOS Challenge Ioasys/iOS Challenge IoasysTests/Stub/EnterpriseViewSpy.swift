//
//  EnterpriseViewSpy.swift
//  iOS Challenge IoasysTests
//
//  Created by Kalil Holanda on 26/11/20.
//

import XCTest
@testable import iOS_Challenge_Ioasys

class EnterpriseViewSpy: EnterpriseViewProtocol {
    var hasCalledErrorRequest = false
    var hasCalledSuccessfulRequest = false
    
    func errorRequest(error: CustomError) {
        hasCalledErrorRequest = true
    }
    
    func successfulRequest() {
        hasCalledSuccessfulRequest = true
    }
}
