//
//  LoginViewStub.swift
//  iOS Challenge IoasysTests
//
//  Created by Kalil Holanda on 24/11/20.
//

import XCTest
@testable import iOS_Challenge_Ioasys

class LoginViewSpy: LoginViewProtocol {
    var hasCalledInvalidEmail = false
    var hasCalledErrorLogin = false
    var hasCalledSuccessfulLogin = false
    
    func invalidEmail() {
        hasCalledInvalidEmail = true
    }
    func errorLogin(error: CustomError) {
        hasCalledErrorLogin = true
    }
    func successfulLogin() {
        hasCalledSuccessfulLogin = true
    }
    
    func setupConstraintsForStartupAnimation() { }
    func startupAnimations() { }
    func setupViewActions() { }
}
