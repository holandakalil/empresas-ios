//
//  EnterpriseServiceTests.swift
//  iOS Challenge IoasysTests
//
//  Created by Kalil Holanda on 26/11/20.
//

// TODO: - Tests for EnterpriseService

import XCTest
@testable import iOS_Challenge_Ioasys

final class EnterpriseServiceTests: XCTestCase {
    
    var sut: EnterpriseService!
    var urlSession: URLSession!
    
    override func setUp() {
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [MockURLProtocol.self]
        urlSession = URLSession(configuration: config)
        sut = EnterpriseService(urlString: Constants.apiUrl, urlSession: urlSession)
    }

    override func tearDown() {
        sut = nil
        urlSession = nil
        
        MockURLProtocol.error = nil
        MockURLProtocol.stubResponseData = nil
    }
}
