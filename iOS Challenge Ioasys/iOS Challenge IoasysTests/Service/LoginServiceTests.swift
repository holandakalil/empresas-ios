//
//  LoginServiceTests.swift
//  iOS Challenge IoasysTests
//
//  Created by Kalil Holanda on 25/11/20.
//

import XCTest
@testable import iOS_Challenge_Ioasys

final class LoginServiceTests: XCTestCase {
    
    var sut: LoginService!
    var urlSession: URLSession!
    
    override func setUp() {
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [MockURLProtocol.self]
        urlSession = URLSession(configuration: config)
        sut = LoginService(urlString: Constants.apiUrl, urlSession: urlSession)
    }

    override func tearDown() {
        sut = nil
        urlSession = nil
        
        MockURLProtocol.error = nil
        MockURLProtocol.stubResponseData = nil
    }

    func testLogin_invalidURL_shouldReturnError() {
        sut = LoginService(urlString: "", urlSession: urlSession)
        let expectation = self.expectation(description: "An empty request URL string expectation")
        
        sut.login(email: "", password: "") { (result) in
            XCTAssertEqual(result, Result.failure(CustomError.invalidRequestURLString), "expected throw for an invalidRequestURLString error")
            expectation.fulfill()
        }
        
        self.wait(for: [expectation], timeout: 5)
    }
    
    func testLogin_invalidResponse_shouldReturnError() {
        let jsonString = "{\"error\":\"Internal Server Error\"}"
        MockURLProtocol.stubResponseData =  jsonString.data(using: .utf8)
        
        let expectation = self.expectation(description: "expectation for a response that contains a different JSON structure")
        
        sut.login(email: "", password: "") { (result) in
            XCTAssertEqual(result, Result.failure(CustomError.invalidResponseModel), "did not return expected error")
            expectation.fulfill()
        }
        
        self.wait(for: [expectation], timeout: 5)
    }
    
    func testLogin_failedRequest_shouldReturnCustomError() {
        MockURLProtocol.error = .failedRequest
        
        let expectation = self.expectation(description: "expectation for a response that contains an error")
        
        sut.login(email: "", password: "") { (result) in
            XCTAssertEqual(result, Result.failure(CustomError.failedRequest), "did not return expected error")
            expectation.fulfill()
        }
        
        self.wait(for: [expectation], timeout: 5)
    }
    
    func testLogin_successfulRequest_shouldReturnSuccess() {
        // TODO: -
    }
}
