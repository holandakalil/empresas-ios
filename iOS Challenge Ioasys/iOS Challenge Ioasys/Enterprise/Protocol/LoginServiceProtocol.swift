//
//  LoginServiceProtocol.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 24/11/20.
//

import Foundation

protocol LoginServiceProtocol {
    init(urlString: String, urlSession: URLSession)
    func login(email: String, password: String, completion: @escaping (Result<UserModel,CustomError>) -> ())
}
