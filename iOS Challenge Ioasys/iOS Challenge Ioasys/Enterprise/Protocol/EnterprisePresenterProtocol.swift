//
//  EnterprisePresenterProtocol.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 26/11/20.
//

import Foundation

protocol EnterprisePresenterProtocol {
    init(enterpriseView: EnterpriseViewProtocol, webservice: EnterpriseServiceProtocol)
    func fetchEnterprises()
    func getEnterprisesCount() -> Int
    func getEnterprise(for row: Int) -> Enterprise?
}
