//
//  ViewCodeProtocol.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 23/11/20.
//

import Foundation

protocol ViewCodeProtocol {
    func buildHierarchy()
    func setupConstraints()
    func configureViews()
}

extension ViewCodeProtocol {
    func applyViewCode() {
        buildHierarchy()
        setupConstraints()
        configureViews()
    }
    
    func configureViews() { }
}
