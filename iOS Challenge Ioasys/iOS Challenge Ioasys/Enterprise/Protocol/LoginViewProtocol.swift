//
//  LoginPresenterProtocol.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 24/11/20.
//

import UIKit

protocol LoginViewBodyProtocol: LoginViewProtocol, UITextFieldDelegate { }

protocol LoginViewProtocol {
    func setupConstraintsForStartupAnimation()
    func startupAnimations()
    func setupViewActions()
    func invalidEmail()
    func errorLogin(error: CustomError)
    func successfulLogin()
}

extension LoginViewProtocol {
    func setupLoginView() {
        setupViewActions()
        setupConstraintsForStartupAnimation()
        startupAnimations()
    }
}
