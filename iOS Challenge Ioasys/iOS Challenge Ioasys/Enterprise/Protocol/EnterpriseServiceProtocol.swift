//
//  EnterpriseServiceProtocol.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 26/11/20.
//

import Foundation

protocol EnterpriseServiceProtocol {
    init(urlString: String, urlSession: URLSession)
    func getEnterprises(completion: @escaping (Result<Enterprises, CustomError>) -> ())
}
