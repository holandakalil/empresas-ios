//
//  LoginPresenterProtocol.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 25/11/20.
//

import Foundation

protocol LoginPresenterProtocol {
    init(loginView: LoginViewProtocol, webservice: LoginServiceProtocol)
    func isValidEmail(email: String) -> Bool
    func isValidPassword(password: String) -> Bool
    func login(email: String, password: String)
}
