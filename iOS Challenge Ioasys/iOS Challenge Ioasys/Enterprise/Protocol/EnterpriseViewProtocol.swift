//
//  EnterpriseViewProtocol.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 26/11/20.
//

import Foundation

protocol EnterpriseViewProtocol {
    func errorRequest(error: CustomError)
    func successfulRequest()
}
