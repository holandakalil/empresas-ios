//
//  LoginPresenter.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 24/11/20.
//

import Foundation

final class LoginPresenter: LoginPresenterProtocol {
    
    private var loginView: LoginViewProtocol
    private var loginServce: LoginServiceProtocol
    
    required init(loginView: LoginViewProtocol, webservice: LoginServiceProtocol = LoginService()) {
        self.loginView = loginView
        self.loginServce = webservice
    }
    
    func isValidEmail(email: String) -> Bool {
        if email.contains("@") && email.contains(".") {
            return true
        }
        return false
    }
    
    func isValidPassword(password: String) -> Bool {
        if password.count > 5 {
            return true
        }
        return false
    }
    
    func login(email: String, password: String) {
        if !isValidEmail(email: email) {
            loginView.invalidEmail()
            return
        }
        
        if !isValidPassword(password: password) {
            loginView.errorLogin(error: .invalidEmail)
            return
        }
        
        // TODO: - Add loading indicator
        loginServce.login(email: email, password: password) { (result) in
            switch result {
                case .success(_):
                    self.loginView.successfulLogin()
                case .failure(let error):
                    self.loginView.errorLogin(error: error)
            }
        }
    }
}
