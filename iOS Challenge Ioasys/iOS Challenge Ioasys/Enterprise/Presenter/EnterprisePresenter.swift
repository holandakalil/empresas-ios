//
//  EnterprisePresenter.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 26/11/20.
//

import Foundation

final class EnterprisePresenter: EnterprisePresenterProtocol {
    
    private var enterpriseView: EnterpriseViewProtocol
    private var enterpriseService: EnterpriseServiceProtocol
    
    private var enterprises = Enterprises(enterprises: [])
    
    required init(enterpriseView: EnterpriseViewProtocol, webservice: EnterpriseServiceProtocol = EnterpriseService()) {
        self.enterpriseView = enterpriseView
        self.enterpriseService = webservice
    }
    
    func fetchEnterprises() {
        // TODO: - Add loading indicator
        enterpriseService.getEnterprises { (result) in
            switch result {
                case .success(let resultEnterprises):
                    self.enterprises = resultEnterprises
                    self.enterpriseView.successfulRequest()
                case .failure(let error):
                    self.enterpriseView.errorRequest(error: error)
            }
        }
    }
    
    func getEnterprisesCount() -> Int {
        return enterprises.enterprises.count
    }
    
    func getEnterprise(for row: Int) -> Enterprise? {
        if getEnterprisesCount() == 0 {
            return nil
        }
        return enterprises.enterprises[row]
    }
}
