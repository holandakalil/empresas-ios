//
//  EnterpriseViewController.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 26/11/20.
//

import UIKit

// TODO: - Improve layout and animations with textField
// TODO: - Search
// TODO: - Details screen

class EnterpriseViewController: UIViewController {
    
    let enterpriseCell = "CellId"
    
    lazy var enterprisePresenter = EnterprisePresenter(enterpriseView: self)
    
    private lazy var backgroundImageview: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.image = UIImage(named: "background")
        return iv
    }()
    
    lazy var searchTextField: UITextField = {
        let tf = UITextField()
        tf.delegate = self
        tf.layer.cornerRadius = 10
        tf.backgroundColor = .systemGray5
        tf.autocorrectionType = .no
        tf.placeholder = "Pesquisar"
        return tf
    }()
    
    lazy var enterprisesTableView: UITableView = {
        let tv = UITableView()
        tv.delegate = self
        tv.dataSource = self
        return tv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        applyViewCode()
        navigationController?.navigationBar.tintColor = .white
        
        enterprisePresenter.fetchEnterprises()
    }
}

extension EnterpriseViewController: ViewCodeProtocol {
    func buildHierarchy() {
        view.addSubview(backgroundImageview)
        view.addSubview(enterprisesTableView)
        view.addSubview(searchTextField)
    }
    
    func setupConstraints() {
        backgroundImageview.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageview.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageview.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImageview.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImageview.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        searchTextField.translatesAutoresizingMaskIntoConstraints = false
        searchTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: -50).isActive = true
        searchTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15).isActive = true
        searchTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15).isActive = true
        searchTextField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        enterprisesTableView.translatesAutoresizingMaskIntoConstraints = false
        enterprisesTableView.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: -22).isActive = true
        enterprisesTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        enterprisesTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        enterprisesTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    func configureViews() {
        enterprisesTableView.separatorStyle = .none
        enterprisesTableView.rowHeight = 80
        enterprisesTableView.register(EnterpriseTableViewCell.self, forCellReuseIdentifier: enterpriseCell)
    }
}

extension EnterpriseViewController: UITextFieldDelegate {
    // TODO: TextField Actions for search
}

extension EnterpriseViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enterprisePresenter.getEnterprisesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: enterpriseCell, for: indexPath) as! EnterpriseTableViewCell
        cell.set(enterprisePresenter.getEnterprise(for: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension EnterpriseViewController: EnterpriseViewProtocol {
    func errorRequest(error: CustomError) {
        switch error {
            case .expiredCredentials:
                DispatchQueue.main.async {
                    self.alert(title: "", message: error.errorDescription, actionTitle: "Ok") {
                        // TODO: - Logout
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            default:
                DispatchQueue.main.async {
                    self.alert(title: "", message: error.errorDescription, actionTitle: "Tentar novamente") {
                        self.enterprisePresenter.fetchEnterprises()
                    }
                }
        }
    }
    
    func successfulRequest() {
        DispatchQueue.main.async {
            self.enterprisesTableView.reloadData()
        }
    }
}
