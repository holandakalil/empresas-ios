//
//  ViewController.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 23/11/20.
//

import UIKit

// TODO: - Improve layout and animations

final class LoginViewController: UIViewController {
    
    private lazy var loginPresenter = LoginPresenter(loginView: self)
    
    private lazy var backgroundImageview: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.image = UIImage(named: "background")
        return iv
    }()
    
    private lazy var logoImageview: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.image = UIImage(named: "logo_white")
        return iv
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Seja bem vindo ao empresas!"
        label.font = UIFont.systemFont(ofSize: 22, weight: .medium)
        label.textColor = .white
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.alpha = 0
        return label
    }()
    
    private lazy var loginBody = LoginViewBody(loginView: self)
    
    private var logoImageviewCenterYAnchor: NSLayoutConstraint?
    private var logoImageviewCenterXAnchor: NSLayoutConstraint?
    private var logoImageviewHeightAnchor: NSLayoutConstraint?
    private var logoImageviewWidthAnchor: NSLayoutConstraint?
    private var loginBodyTopAnchor: NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        applyViewCode()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupLoginView()
    }
}

extension LoginViewController: ViewCodeProtocol {
    func buildHierarchy() {
        view.addSubview(backgroundImageview)
        view.addSubview(logoImageview)
        view.addSubview(descriptionLabel)
        view.addSubview(loginBody)
    }
    
    func setupConstraints() {
        backgroundImageview.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageview.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageview.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImageview.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImageview.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        logoImageview.translatesAutoresizingMaskIntoConstraints = false
        logoImageviewCenterYAnchor = logoImageview.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        logoImageviewCenterXAnchor = logoImageview.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        logoImageviewHeightAnchor = logoImageview.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 3/5)
        logoImageviewWidthAnchor = logoImageview.heightAnchor.constraint(equalTo: logoImageview.widthAnchor, multiplier: 2/1)
        logoImageviewCenterYAnchor?.isActive = true
        logoImageviewCenterXAnchor?.isActive = true
        logoImageviewHeightAnchor?.isActive = true
        logoImageviewWidthAnchor?.isActive = true
        
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.topAnchor.constraint(equalTo: logoImageview.bottomAnchor, constant: 15).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15).isActive = true
        
        loginBody.translatesAutoresizingMaskIntoConstraints = false
        loginBody.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        loginBody.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        loginBody.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        loginBodyTopAnchor = loginBody.topAnchor.constraint(equalTo: view.bottomAnchor)
        loginBodyTopAnchor?.isActive = true
    }
}

extension LoginViewController: LoginViewBodyProtocol {
    func setupViewActions() {
        loginBody.loginButton.addTarget(self, action: #selector(login), for: .touchUpInside)
    }
    
    @objc func login() {
        loginPresenter.login(email: loginBody.loginTextField.text ?? "", password: loginBody.passwordTextField.text ?? "")
    }
    
    func setupConstraintsForStartupAnimation() {
        logoImageviewCenterYAnchor?.isActive = false
        logoImageviewCenterXAnchor?.isActive = false
        logoImageviewHeightAnchor?.isActive = false
        logoImageviewWidthAnchor?.isActive = false
        
        logoImageview.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: -50).isActive = true
        logoImageview.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImageview.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1/5).isActive = true
        logoImageview.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1/5).isActive = true
        
        loginBodyTopAnchor?.isActive = false
        loginBody.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 15).isActive = true
    }
    
    func startupAnimations() {
        UIView.transition(with: logoImageview, duration: 1, options: .transitionFlipFromBottom) {
            self.logoImageview.image = UIImage(named: "icon_logo_white")
        } completion: { (_) in }

        UIView.animate(withDuration: 1) {
            self.descriptionLabel.alpha = 1
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - TextField
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    func invalidEmail() {
        loginBody.loginTextField.setRedBorder()
    }
    
    func errorLogin(error: CustomError) {
        loginBody.loginTextField.setRedBorder()
        loginBody.passwordTextField.setRedBorder()
    }
    
    func successfulLogin() {
        let enterpriseVC = EnterpriseViewController()
        
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(enterpriseVC, animated: true)
        }
    }
}
