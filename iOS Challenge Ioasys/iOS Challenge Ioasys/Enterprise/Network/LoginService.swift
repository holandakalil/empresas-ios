//
//  LoginService.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 25/11/20.
//

import Foundation

final class LoginService: LoginServiceProtocol {
    
    private var urlSession: URLSession
    private var urlString: String
    
    required init(urlString: String = Constants.apiUrl + "users/auth/sign_in",
         urlSession: URLSession = .shared) {
        self.urlString = urlString
        self.urlSession = urlSession
    }
    
    func login(email: String, password: String, completion: @escaping (Result<UserModel, CustomError>) -> ()) {
        guard let url = URL(string: urlString) else {
            completion(.failure(.invalidRequestURLString))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = Constants.timeOutTime
        request.httpBody = try? JSONEncoder().encode(UserModel(email: email, password: password))
        
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            if let _ = error {
                completion(.failure(.failedRequest))
                return
            }
            
            guard let httpUrlResponse = response as? HTTPURLResponse else {
                completion(.failure(.invalidResponseModel))
                return
            }
            
            if let accessToken = httpUrlResponse.allHeaderFields["access-token"] as? String,
               let client = httpUrlResponse.allHeaderFields["client"] as? String,
               let uid = httpUrlResponse.allHeaderFields["uid"] as? String {
                User.shared.save(user: UserModel(email: email, password: password))
                User.shared.save(auth: UserAuth(accessToken: accessToken, client: client, uid: uid))
                completion(.success(User.shared.current()))
            } else {
                completion(.failure(.invalidResponseModel))
            }
        }
        
        dataTask.resume()
    }
    
}
