//
//  EnterpriseService.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 26/11/20.
//

import Foundation

final class EnterpriseService: EnterpriseServiceProtocol {
    
    private var urlSession: URLSession
    private var urlString: String
    
    required init(urlString: String = Constants.apiUrl + "enterprises/",
         urlSession: URLSession = .shared) {
        self.urlString = urlString
        self.urlSession = urlSession
    }
    
    func getEnterprises(completion: @escaping (Result<Enterprises, CustomError>) -> ()) {
        guard let url = URL(string: urlString) else {
            completion(.failure(.invalidRequestURLString))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(User.shared.auth().accessToken, forHTTPHeaderField: UserAuth.key.accessToken.rawValue)
        request.setValue(User.shared.auth().client, forHTTPHeaderField: UserAuth.key.client.rawValue)
        request.setValue(User.shared.auth().uid, forHTTPHeaderField: UserAuth.key.uid.rawValue)
        request.timeoutInterval = Constants.timeOutTime
        
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            if let _ = error {
                completion(.failure(.failedRequest))
                return
            }
            
            guard let httpURLResponse = response as? HTTPURLResponse else {
                completion(.failure(.invalidResponseModel))
                return
            }
            
            if httpURLResponse.statusCode == 401 {
                completion(.failure(.expiredCredentials))
                return
            }
            
            if let data = data,
               let response = try? JSONDecoder().decode(Enterprises.self, from: data) {
                completion(.success(response))
            } else {
                completion(.failure(.invalidResponseModel))
            }
        }
        
        dataTask.resume()
    }
    
}
