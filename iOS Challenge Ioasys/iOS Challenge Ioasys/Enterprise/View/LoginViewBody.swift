//
//  LoginView.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 24/11/20.
//

import UIKit

class LoginViewBody: UIView {
    
    var loginView: LoginViewBodyProtocol
    init(loginView: LoginViewBodyProtocol) {
        self.loginView = loginView
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var loginLabel: UILabel = {
        let label = UILabel()
        label.text = "Email"
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = .darkText
        return label
    }()
    
    lazy var passwordLabel: UILabel = {
        let label = UILabel()
        label.text = "Senha"
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = .darkText
        return label
    }()
    
    lazy var loginTextField: UITextField = {
        let tf = UITextField()
        tf.delegate = loginView
        tf.keyboardType = .emailAddress
        tf.layer.cornerRadius = 10
        tf.backgroundColor = .systemGray5
        tf.autocorrectionType = .no
        return tf
    }()
    
    lazy var passwordTextField: UITextField = {
        let tf = UITextField()
        tf.delegate = loginView
        tf.isSecureTextEntry = true
        tf.layer.cornerRadius = 10
        tf.backgroundColor = .systemGray5
        tf.autocorrectionType = .no
        return tf
    }()
    
    lazy var loginButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("Entrar", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        btn.backgroundColor = UIColor(named: "pink")
        btn.layer.cornerRadius = 10
        return btn
    }()
    
    func setup() {
        backgroundColor = .white
        
//        setupCurvedTopMask() // FIXME: not working
        applyViewCode()
    }
    
    func setupCurvedTopMask() {
        isOpaque = false
        
        let path = UIBezierPath()
        path.move(to: CGPoint.init(x: frame.minX, y: frame.minY))
        path.addLine(to: CGPoint.init(x: frame.minX, y: frame.minY))
        path.addCurve(to: CGPoint.init(x: frame.maxX, y: frame.minY), controlPoint1: CGPoint.init(x: frame.midX - 20, y: frame.minY + 50), controlPoint2: CGPoint.init(x: frame.midX + 20, y: frame.minY + 50))
        path.addLine(to: CGPoint.init(x: frame.maxX, y: frame.minY))
        path.close()
        
        let fillColor = UIColor.red
        fillColor.setFill()
        path.fill()
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = bounds
        shapeLayer.path = path.cgPath
        shapeLayer.fillColor = fillColor.cgColor
        shapeLayer.fillRule = .nonZero
        layer.addSublayer(shapeLayer)
    }
}

extension LoginViewBody: ViewCodeProtocol {
    func buildHierarchy() {
        addSubview(loginLabel)
        addSubview(loginTextField)
        addSubview(passwordLabel)
        addSubview(passwordTextField)
        addSubview(loginButton)
    }
    
    func setupConstraints() {
        loginLabel.translatesAutoresizingMaskIntoConstraints = false
        loginLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        loginLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        loginLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
        
        loginTextField.translatesAutoresizingMaskIntoConstraints = false
        loginTextField.topAnchor.constraint(equalTo: loginLabel.bottomAnchor, constant: 8).isActive = true
        loginTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        loginTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
        loginTextField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        passwordLabel.translatesAutoresizingMaskIntoConstraints = false
        passwordLabel.topAnchor.constraint(equalTo: loginTextField.bottomAnchor, constant: 15).isActive = true
        passwordLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        passwordLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
        
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextField.topAnchor.constraint(equalTo: passwordLabel.bottomAnchor, constant: 8).isActive = true
        passwordTextField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
        passwordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 45).isActive = true
        loginButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 25).isActive = true
        loginButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
    func configureViews() {
        loginTextField.tag = 0
        passwordTextField.tag = 1
    }
}
