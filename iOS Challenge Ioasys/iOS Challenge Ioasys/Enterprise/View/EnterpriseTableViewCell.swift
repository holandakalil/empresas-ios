//
//  EnterpriseTableViewCell.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 26/11/20.
//

// TODO: - Improve UI

import UIKit

class EnterpriseTableViewCell: UITableViewCell {
    
    var nameLabel = UILabel()
    var cityLabel = UILabel()
    
    lazy var guide = safeAreaLayoutGuide
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(nameLabel)
        addSubview(cityLabel)
        setupNameLabel()
        setupCityLabel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupNameLabel() {
        nameLabel.numberOfLines = 1
        nameLabel.textColor = .darkText
        nameLabel.adjustsFontSizeToFitWidth = true
        nameLabel.font = UIFont.preferredFont(forTextStyle: .title3)
        
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 12).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: -12).isActive = true
    }
    
    func setupCityLabel() {
        cityLabel.numberOfLines = 1
        cityLabel.textColor = .darkText
        cityLabel.adjustsFontSizeToFitWidth = true
        cityLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
        
        cityLabel.translatesAutoresizingMaskIntoConstraints = false
        cityLabel.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 12).isActive = true
        cityLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 5).isActive = true
        cityLabel.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: -12).isActive = true
    }
    
    func set(_ enterprise: Enterprise?) {
        guard let enterprise = enterprise else {
            nameLabel.text = ""
            cityLabel.text = ""
            return
        }
        nameLabel.text = enterprise.enterprise_name
        cityLabel.text = enterprise.city ?? ""
    }

}
