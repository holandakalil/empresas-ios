//
//  User.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 26/11/20.
//

import Foundation

struct UserAuth {
    let accessToken: String
    let client: String
    let uid: String
    
    enum key: String {
        case accessToken = "access-token"
        case client = "client"
        case uid = "uid"
    }
}
