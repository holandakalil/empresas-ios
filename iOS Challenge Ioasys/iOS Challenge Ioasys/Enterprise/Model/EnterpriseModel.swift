//
//  Enterprise.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 26/11/20.
//

import Foundation

struct Enterprises: Decodable {
    let enterprises: [Enterprise]
}

struct Enterprise: Decodable {
    let id: Int
    let enterprise_name: String
    let description: String?
    let photo: String?
    let city: String?
    let enterprise_type: EnterpriseType?
    
    struct EnterpriseType: Decodable {
        let id: Int
        let enterprise_type_name: String
    }
}
