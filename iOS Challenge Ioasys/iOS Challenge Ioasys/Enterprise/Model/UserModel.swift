//
//  User.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 24/11/20.
//

import Foundation

import Foundation

struct UserModel: Encodable, Decodable, Equatable {
    let email: String
    let password: String
}
