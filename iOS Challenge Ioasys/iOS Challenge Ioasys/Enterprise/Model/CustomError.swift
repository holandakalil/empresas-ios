//
//  CustomError.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 24/11/20.
//

import Foundation

enum CustomError: LocalizedError, Equatable { // TODO: - add more errors
    case invalidResponseModel
    case invalidRequestURLString
    case failedRequest
    case invalidEmail
    case expiredCredentials
    
    var errorDescription: String {
        switch self {
            case .failedRequest: return "Houve um erro 😰"
            case .invalidRequestURLString: return "URL inválida"
            case .invalidResponseModel: return "Erro, tente novamente"
            case .invalidEmail: return "E-mail inválido."
            case .expiredCredentials: return "Seu acesso expirou"
        }
    }
}
