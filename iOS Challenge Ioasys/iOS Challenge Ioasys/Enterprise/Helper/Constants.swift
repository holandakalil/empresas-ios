//
//  Constants.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 23/11/20.
//

import Foundation

struct Constants {
    // WS
    static let apiVersion = "v1"
    static let apiUrl = "https://empresas.ioasys.com.br/api/\(apiVersion)/"
    static let timeOutTime = 5.0
}
