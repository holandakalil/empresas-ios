//
//  UITextField+Extension.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 26/11/20.
//

import UIKit

extension UITextField {
    func setRedBorder() {
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.red.withAlphaComponent(0.4).cgColor
    }
}
