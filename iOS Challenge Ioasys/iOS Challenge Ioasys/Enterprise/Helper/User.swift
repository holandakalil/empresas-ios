//
//  User.swift
//  iOS Challenge Ioasys
//
//  Created by Kalil Holanda on 26/11/20.
//

import Foundation

class User {  // TODO: - Save to KeyChain
    static let shared = User()

    private init() { }
    
    private var currentUser = UserModel(email: "", password: "")
    private var userAuth = UserAuth(accessToken: "", client: "", uid: "")
    
    func save(user: UserModel) {
        currentUser = user
    }
    
    func save(auth: UserAuth) {
        userAuth = auth
    }
    
    func current() -> UserModel {
        return currentUser
    }
    
    func auth() -> UserAuth {
        return userAuth
    }
    
    func logout() {
        currentUser = UserModel(email: "", password: "")
        userAuth = UserAuth(accessToken: "", client: "", uid: "")
    }
}
